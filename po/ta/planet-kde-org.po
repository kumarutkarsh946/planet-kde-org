#
# Kishore G <kishore96@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:52+0000\n"
"PO-Revision-Date: 2023-04-16 16:17+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.90\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr ""

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""

#: config.yaml:0
msgid "Add your own feed"
msgstr "உங்கள் தொகுப்பை சேர்ப்பது"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "ஒரு பதிவளவு கீழே செல்"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "ஒரு பதிவளவு மேலே செல்"
