#
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:52+0000\n"
"PO-Revision-Date: 2022-04-25 00:52+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr ""

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""

#: config.yaml:0
msgid "Add your own feed"
msgstr ""

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr ""

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr ""
